Unity game made following Blackthornprod's YouTube tutorial on random level generation:
https://www.youtube.com/watch?v=hk6cUanSfXQ

Made with the open license or whatever. Anyone can use any of the assets I created without attributing me.

Using assets from:

* Purchased from itch.io:
    * Kenney's Game Assets Pack - https://kenney.itch.io/
        * Floors & Laser - 2D platformer building assets 
* Free art from OpenGameArt.org
    * Laser beams - https://opengameart.org/content/lasers-and-beams