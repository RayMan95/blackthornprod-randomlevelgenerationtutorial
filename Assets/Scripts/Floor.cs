﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floor : MonoBehaviour
{
    public Sprite[] sprites;
    
    void Start()
    {
        int rand = Random.Range(0, sprites.Length);
        this.GetComponent<SpriteRenderer>().sprite = sprites[rand];
    }
}
