﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGeneration : MonoBehaviour
{
    public GameObject roomsParent;
    public GameObject closedRoomPrefab;
    
    [SerializeField] public LayerMask roomMask;
    private Dictionary<Vector3, GameObject> level = new Dictionary<Vector3, GameObject>();

    private LinkedList<GameObject> criticalPath = new LinkedList<GameObject>();

    private int direction;
    private int downCounter = 0;
    private float moveAmount;
    private float minX = -10f;
    private float maxX = 50f;
    private float minY = -70f;
    private float maxY = -10f;

    private float timeBtwnRoom;
    public float startTimeBtwnRoom = 0.25f;
    private bool makingCriticalPath = false;
    // private bool stopGeneration = false;

    public List<int[]> spawnPointReductions;

    void Awake()
    {
        // moveAmount = GameControl.instance.roomSize;
        moveAmount = 20;
    }

    void Start()
    {
        Debug.Log("Start: " + gameObject);

        MakeCriticalPath();
        FillRooms();
        OpenRooms();
        Destroy(gameObject);
    }

    private void OpenRooms()
    {
        Debug.Log("Opening rooms");

        foreach (GameObject roomObject in level.Values)
        {
            roomObject.GetComponent<Room>().OpenSides();
        }
    }

    void MakeCriticalPath()
    {
        makingCriticalPath = true;
        Debug.Log("Making critical path");

        Vector3[] startingPositions = {new Vector3(minX,maxY,0f), new Vector3(minX+moveAmount,maxY,0f), new Vector3(minX+(moveAmount*2),maxY,0f), new Vector3(maxX,maxY,0f)};
        
        transform.position = startingPositions[Random.Range(0,3)];

        AddRoom(true);

        AddOpening(transform.position, 16);


        direction = Random.Range(1,5);
        timeBtwnRoom = startTimeBtwnRoom;

        while (makingCriticalPath)
        {
            if (timeBtwnRoom <= 0)
            {
                Move();
                timeBtwnRoom = startTimeBtwnRoom;
            }
            else
            {
                timeBtwnRoom -= Time.deltaTime;
            }
        }

        Debug.Log("CriticalPath: " + criticalPath.Count);
    }

    private GameObject AddRoom(bool critical)
    {
        GameObject instance;

        // Check if room at location exists
        level.TryGetValue(transform.position, out instance);
        if(instance != null)
        {
            return null;
        }
        
        instance = (GameObject)Instantiate(closedRoomPrefab, transform.position, Quaternion.identity);
        instance.transform.parent = roomsParent.transform;

        // instance.AddComponent<Room>();
        Room room = instance.GetComponent<Room>();
        room.onCriticalPath = critical;
        room.MakeAllSpawnPoints();

        if(critical)
        {
            criticalPath.AddLast(instance); 
        }
        
        level.Add(transform.position, instance);
        return instance;
    }

    // TODO: either make it so that it doesn't go down multiple levels or add flags to identify multiple downs 
    private void Move()
    {
        Vector2 oldPos = transform.position;
        if (direction == 1 || direction == 2) // Move right
        {
            if(transform.position.x < maxX)
            {
                downCounter = 0;
                
                // Add opening before moving
                AddOpening(transform.position, 2);

                Vector2 newPos = new Vector2(transform.position.x + moveAmount, transform.position.y);
                transform.position = newPos;

                AddRoom(true);
                // Add opening after moving
                AddOpening(transform.position, 1);

                direction = Random.Range(1,5);
                if (direction == 3)
                {
                    direction = 2;
                }
                else if(direction == 4)
                {
                    direction = 5;
                }
            }
            else
            {
                direction = 5;
            }
        }
        else if (direction == 3 || direction == 4) // Move left
        {
            if(transform.position.x > minX)
            {
                downCounter = 0;

                // Add opening before moving
                AddOpening(transform.position, 1);

                Vector2 newPos = new Vector2(transform.position.x - moveAmount, transform.position.y);
                transform.position = newPos;
                
                AddRoom(true);
                // Add opening after moving
                AddOpening(transform.position, 2);

                direction = Random.Range(3,5);
            }
            else
            {
                direction = 5;
            }
        }
        else if (direction == 5) // Move down
        {
            downCounter++;

            if(transform.position.y > minY)
            {
                // Add opening before moving
                AddOpening(transform.position, 8);

                Vector2 newPos = new Vector2(transform.position.x, transform.position.y - moveAmount);
                transform.position = newPos;

                // Add opening after moving
                AddRoom(true);
                AddOpening(transform.position, 4);

                direction = Random.Range(1,5);
            }
            else
            {
                // done
                makingCriticalPath = false;
                
                AddOpening(transform.position, 32);
            }
        }        
    }

    private void AddOpening(Vector3 position, int openingType)
    {
        GameObject roomGameObject;
        level.TryGetValue(position, out roomGameObject);
        roomGameObject.GetComponent<Room>().type += openingType;
    }

    private void FillRooms()
    {
        for (float y = minY; y <= maxY; y += moveAmount)
        {
            for (float x = minX; x <= maxX; x += moveAmount)
            {
                transform.position = new Vector3(x, y, 0f);
                GameObject room;
                level.TryGetValue(transform.position, out room);
                if(room == null)
                {
                    AddRoom(false);
                }
            }
        }
    }


    // Returns bitmask of neighbours (will map to room type later)
    // returns 0 for no neighbours and null if invalid address 
    private Room.Type FindCriticalNeighbours(Vector3 position, bool isCritical)
    {
        if (!WithinBounds(position)) return Room.Type.fail;

        Room.Type result = 0;
        GameObject room;

        Vector3 modified = position;
        modified.x -= 10; // left

        if (level.TryGetValue(modified, out room) && room.GetComponent<Room>().onCriticalPath == isCritical) result += 1; // left

        modified = position;
        modified.x += 10; // right

        if (level.TryGetValue(modified, out room) && room.GetComponent<Room>().onCriticalPath == isCritical) result += 2; // right

        modified = position;
        modified.y += 10; // up

        if (level.TryGetValue(modified, out room) && room.GetComponent<Room>().onCriticalPath == isCritical) result += 4; // up

        modified = position;
        modified.y -= 10; // down
        
        if (level.TryGetValue(modified, out room) && room.GetComponent<Room>().onCriticalPath == isCritical)
        {
            // needs to have at most 1 flag set prior
            if (!(result.HasFlag(Room.Type.r)) && !(result.HasFlag(Room.Type.l)) || result == 0)
                result += 8; // down
        }
        return result;
    }

    private bool WithinBounds(Vector3 position)
    {
        return !(position.x < minX || position.x > maxX || position.y < minY || position.y > maxY);
    }

    private bool IsCornerBlock(Vector3 position)
    {
        if (position.x == minX && position.y == minY) return true;
        else if (position.x == minX && position.y == maxY) return true;
        else if (position.x == maxX && position.y == minY) return true;
        else if (position.x == maxX && position.y == maxY) return true;

        return false;
    }
}
