﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room : MonoBehaviour
{

    // TODO add entrance + exit variables
    public Type type;
    public bool onCriticalPath;

    public GameObject spawnPointPrefab;
    private Dictionary<Vector3, GameObject> borderSpawnPoints; // {tranform_absolute_position : spawn_object}
    private int sideLength;

    void Awake()
    {
        // sideLength = GameControl.instance.roomSize;
        sideLength = 20;
    }

    public enum Type
    {
        fail = -1,
        n = 0,
        l = 1, 
        r = 2,
        u = 4,
        d = 8,
        en = 16,
        ex = 32
    }

    public void MakeAllSpawnPoints()
    {
        // Max spawns are side lengths * 4 - 4
        int max = (sideLength * 4) - 4;

        borderSpawnPoints = new Dictionary<Vector3, GameObject>();

        float startPositionX = -(sideLength/2) + 0.5f;
        float startPositionY = -(sideLength/2) + 0.5f;
        float endPositionX = (sideLength/2) - 0.5f;
        float endPositionY = (sideLength/2) - 0.5f;

        // Do 0 pos first coz of overlap
        MakeSpawnPoint(startPositionX, startPositionY);
        MakeSpawnPoint(startPositionX, endPositionY);
        MakeSpawnPoint(endPositionX, startPositionY);
        MakeSpawnPoint(endPositionX, endPositionY);
        
        int jump = 1; // start loop at 1 coz of above

        while (jump < sideLength-1) // -1 coz end is start of other side
        {
            MakeSpawnPoint(startPositionX + jump, startPositionY);
            MakeSpawnPoint(startPositionX, startPositionY + jump);
            MakeSpawnPoint(endPositionX - jump, endPositionY);
            MakeSpawnPoint(endPositionX, endPositionY - jump);

            jump++;
        }
            
    }

    private void MakeSpawnPoint(float x, float y)
    {
        float relativeX = transform.position.x + x;
        float relativeY = transform.position.y + y;

        Vector3 spawnPos = new Vector3(relativeX, relativeY, 0f);

        GameObject newSpawnPoint = (GameObject)Instantiate(spawnPointPrefab, spawnPos, Quaternion.identity);
        newSpawnPoint.transform.parent = transform;
        borderSpawnPoints.Add(spawnPos, newSpawnPoint);
    }

    public void OpenSides()
    {
        float start = (-sideLength)/2 + 0.5f;
        float end =  sideLength/2 - 0.5f;
        float offset = 4f;

        Vector3 pos = new Vector3(0f, 0f, 0f);

        
        for (float i = start + offset; i <= end-offset; i += 1f)
        {
            if (type.HasFlag(Type.r))
            {
                pos.x = transform.position.x + end;
                pos.y = transform.position.y + i;
                this.RemoveSpawnPoint(pos);
            }
            if (type.HasFlag(Type.l))
            {
                pos.x = transform.position.x + start;
                pos.y = transform.position.y + i;
                this.RemoveSpawnPoint(pos);
            }
            if (type.HasFlag(Type.u))
            {
                pos.x = transform.position.x + i;
                pos.y = transform.position.y + end;
                this.RemoveSpawnPoint(pos);
            }
            if (type.HasFlag(Type.d))
            {
                pos.x = transform.position.x + i;
                pos.y = transform.position.y + start;
                this.RemoveSpawnPoint(pos);
            }            
        }
        Debug.Log("Stop here");
    }

    // returns true if valid index and object at index
    public bool RemoveSpawnPoint(Vector3 relativePosition)
    {
        GameObject spawnPoint;
        borderSpawnPoints.TryGetValue(relativePosition, out spawnPoint);
        if (spawnPoint != null)
        {
            Destroy(spawnPoint);
            borderSpawnPoints.Remove(relativePosition);
            return true;
        }
        else return false;
    }
}
